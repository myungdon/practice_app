import 'package:flutter/material.dart';
import 'package:practice_app/pages/page_login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  static Future<String?> getRiderToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberToken');
  }
  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }
  static void setRiderToken(String memberToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberToken', memberToken);
  }
  static void setRiderName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (BuildContext context) => const PageLogin()),
          (route) => false
    );
  }
}