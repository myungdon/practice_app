import 'package:flutter/material.dart';
import 'package:practice_app/login_check.dart';
import 'package:practice_app/pages/page_ask.dart';
import 'package:practice_app/pages/page_board_post.dart';
import 'package:practice_app/pages/page_board_post2.dart';
import 'package:practice_app/pages/page_index.dart';
import 'package:practice_app/pages/page_location.dart';
import 'package:practice_app/pages/page_login.dart';
import 'package:practice_app/pages/page_navigator.dart';
import 'package:practice_app/pages/page_notice_detail.dart';
import 'package:practice_app/pages/page_notice_list.dart';
import 'package:practice_app/pages/page_order_request.dart';
import 'package:practice_app/pages/page_screen_a.dart';
import 'package:practice_app/pages/page_screen_b.dart';
import 'package:practice_app/pages/page_screen_c.dart';
import 'package:practice_app/pages/page_test1.dart';
import 'package:practice_app/pages/page_test2.dart';
import 'package:practice_app/pages/page_test3.dart';
import 'package:practice_app/pages/page_toast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      initialRoute: "/location", // 시작 주소
      routes: { // 페이지 주소 경로
        "/index": (context) => PageIndex(),
        "/test1": (context) => PageTest1(),
        "/test2": (context) => PageTest2(),
        "/test3": (context) => PageTest3(),
        "/ask": (context) => PageAsk(),
        "/toast": (context) => PageToast(),
        "/navigator": (context) => PageNavigator(),
        "/screen-a": (context) => PageScreenA(),
        "/screen-b": (context) => PageScreenB(),
        "/screen-c": (context) => PageScreenC(),
        // "/notice-list": (context) => PageNoticeList(),
        "/order_request": (context) => PageOrderRequest(),
        "/login": (context) => PageLogin(),
        "/login-check": (context) => LoginCheck(),
        "/board-post": (context) => PageBoardPost(),
        // "/board-post2": (context) => PageBoardPost2(formData: ,),
        // "/board": (context) => PageBoard(),
        // "/board-detail": (context) => PageBoardDetail(),
        "/notice-list": (context) => PageNoticeList(),
        "/page_notice_detail": (context) => PageNoticeDetail(id: 1),
        "/location": (context) => PageLocation(),
      },
      // 디버그 리본 삭제
      debugShowCheckedModeBanner: false,
    );
  }
}