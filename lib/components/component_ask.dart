import 'package:flutter/material.dart';
import 'package:practice_app/model/ask_item.dart';

class ComponentAsk extends StatelessWidget {
  const ComponentAsk({super.key, required this.askItem, required this.callback});

  final VoidCallback callback;
  final AskItem askItem;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          SizedBox(height: 50),
          Text(askItem.name),
        ],
      ),
    );
  }
}
