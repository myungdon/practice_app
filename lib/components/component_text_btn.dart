import 'package:flutter/material.dart';

class ComponentTextBtn extends StatelessWidget {
  final String text;
  final Color bgColor;
  final Color textColor;
  final Color borderColor;
  final VoidCallback callback;
  const ComponentTextBtn(this.text, this.callback, {Key? key, this.bgColor = Colors.black, this.textColor = Colors.red, this.borderColor = Colors.blue}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: callback,
        child: Text(text),
        style: ElevatedButton.styleFrom(
          primary: bgColor,
          onPrimary: textColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(3.0),
            side: BorderSide(
              color: borderColor,
            )
          )
        ),
    );
  }
}
