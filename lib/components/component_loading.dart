import 'package:flutter/material.dart';

class CoponentLoading extends StatefulWidget {
  const CoponentLoading({super.key});

  @override
  State<CoponentLoading> createState() => _CoponentLoadingState();
}

class _CoponentLoadingState extends State<CoponentLoading> {
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
