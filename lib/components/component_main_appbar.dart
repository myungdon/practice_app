// 앱바

import 'package:flutter/material.dart';
import 'package:practice_app/config/config_color.dart';
import 'package:practice_app/config/config_path.dart';
import 'package:practice_app/config/config_size.dart';

class ComponentMainAppbar extends StatelessWidget implements PreferredSizeWidget{
  const ComponentMainAppbar({super.key, required this.center, required this.appBar});

  final AppBar appBar;
  final bool center;

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;

    return AppBar(
      centerTitle: center,
      backgroundColor: colorSecondary,
      title: Image.asset('${pathBase}logo2.png',
        width: _size.width * 0.17,
        height: _size.height * 0.17,),
      actions: [
        IconButton(onPressed: () {}, icon: Icon(Icons.shopping_cart)),
        IconButton(onPressed: () {}, icon: Icon(Icons.search))
        ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}
