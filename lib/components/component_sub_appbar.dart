// 앱바

import 'package:flutter/material.dart';
import 'package:practice_app/config/config_color.dart';
import 'package:practice_app/config/config_path.dart';
import 'package:practice_app/config/config_size.dart';

class ComponentSubAppbar extends StatelessWidget implements PreferredSizeWidget{
  const ComponentSubAppbar({super.key, required this.title, required this.center, required this.appBar});

  final AppBar appBar;
  final String title;
  final bool center;

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;

    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.arrow_back, color: colorPrimary,),
        onPressed: () => Navigator.of(context).pop(),
      ),
      centerTitle: center,
      backgroundColor: colorSecondary,
      title: Text("$title"),
      // actions: [
      //   Text("$title", style: TextStyle(
      //     color: colorPrimary,
      //     fontSize: 18.0,
      //     fontWeight: fontWeightBig,
      //   ),
      //   ),
      //   SizedBox(width: 20,)
      // ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}
