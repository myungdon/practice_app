// 구분선

import 'package:flutter/material.dart';

class ComponentDivider extends StatelessWidget {
  const ComponentDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return Divider(
      height: 30, // 구분선 위아래 사이즈
      color: Colors.grey[400], // 색깔
      thickness: 1, // 선 두께
      endIndent: 30, // 내어 쓰기
    );
  }
}
