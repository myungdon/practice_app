import 'package:flutter/material.dart';
import 'package:practice_app/model/notice_item.dart';

class ComponentNotice extends StatelessWidget {
  const ComponentNotice({
    super.key,
    required this.noticeItem,
    required this. callback,

  });

  final NoticeItem noticeItem;
  final VoidCallback callback;




  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: [
          Container(
            alignment: AlignmentDirectional.bottomStart,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /** 컴포넌트 시작 **/
                Container(
                  alignment: AlignmentDirectional.bottomStart,
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        noticeItem.title,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: 16,
                            color: Colors.amberAccent,
                            letterSpacing: -0.5
                        ),
                      ),
                      Text(
                        noticeItem.dateCreate,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontFamily:'NotoSans_NotoSansKR-Light',
                            fontSize: 16,
                            color: Colors.green
                        ),
                      ),
                    ],
                  ),
                ),
                /** 구분선 **/
                Container(
                  height: 1,
                  color: Color.fromRGBO(190, 190, 190, 0.2),
                ),
              ],
            ),
          ),
        ],
      ),
      onTap: () {

        // @TODO 리스트 클릭했을 때 클릭한 놈의 아이디 가져오기

        // Navigator.pushNamed(
        //   context,
        //   ExtractArgumentsScreen.routeName,
        //   arguments: ScreenArguments(
        //     'Extract Arguments Screen',
        //     'This message is extracted in the build method.',
        //   ),
        // );
        Navigator.of(context).pushNamed("/page_notice_detail");
      },
    );
  }
}

