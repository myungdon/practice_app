class DeliveryResponse{
  num id;
  String name;

  DeliveryResponse(this.id, this.name);

  factory DeliveryResponse.fromJson(Map<String, dynamic> json) {
    return DeliveryResponse(json['id'], json['name']);
  }
}