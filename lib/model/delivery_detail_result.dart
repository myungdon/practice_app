import 'package:practice_app/model/delivery_response.dart';

class DeliveryDetailResult {
  DeliveryResponse data;

  DeliveryDetailResult(this.data);

  factory DeliveryDetailResult.fromJson(Map<String, dynamic> json) {
    return DeliveryDetailResult(
      DeliveryResponse.fromJson(json['data'])
    );
  }
}