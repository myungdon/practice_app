import 'package:practice_app/model/notice_item.dart';

class NoticeListResult{
  num totalCount;
  num totalPage;
  num currantPage;
  List<NoticeItem> list;

  NoticeListResult(
      this.totalCount,
      this.totalPage,
      this.currantPage,
      this.list
      );

  factory NoticeListResult.fromJson(Map<String,dynamic> json) {

    return NoticeListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null
          ? (json['list'] as List ).map((e) => NoticeItem.fromJson(e)).toList()
          : [],
    );
  }




}
