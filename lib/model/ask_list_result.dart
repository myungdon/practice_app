import 'package:practice_app/model/ask_item.dart';

class AskListResult{
  List<AskItem> list;
  num totalCount;

  AskListResult(this.list, this.totalCount);

  factory AskListResult.fromJson(Map<String, dynamic> json) {
    return AskListResult(
        json['list'] != null ? (json['list'] as List).map((e) => AskItem.fromJson(e)).toList() : [],
        json['totalCount']
    );
  }
}