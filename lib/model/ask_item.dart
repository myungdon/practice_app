class AskItem {
  num id;
  String name;

  AskItem(this.id, this.name);

  factory AskItem.fromJson(Map<String, dynamic> json) {
    return AskItem(json['id'], json['name']);
  }
}