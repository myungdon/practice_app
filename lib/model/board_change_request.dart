class BoardChangeRequest {
  String title;
  String text;
  String? img;

  BoardChangeRequest(this.title, this.text, [this.img]);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['title'] = this.title;
    data['text'] = this.text;
    data['img'] = this.img;
    return data;
  }
}