class RiderResponse{
  num id;
  String name;
  String password;
  String addressWish;
  String driveType;
  String driveNumber;
  String phoneType;
  String bankName;

  RiderResponse(
      this.id,
      this.name,
      this.password,
      this.addressWish,
      this.driveType,
      this.driveNumber,
      this.phoneType,
      this.bankName
      );

  factory RiderResponse.fromJson(Map<String,dynamic>json) {
    return RiderResponse(
        json['id'],
        json['name'],
        json['password'],
        json['addressWish'],
        json['driveType'],
        json['driveNumber'],
        json['phoneType'],
        json['bankName']
    );
  }

}

