import 'package:practice_app/model/rider_response.dart';

class SingleResultRiderResponse {
  String msg;
  num code;
  bool isSuccess;
  RiderResponse data;

  SingleResultRiderResponse(
      this.msg,
      this.code,
      this.isSuccess,
      this.data
      );

  factory SingleResultRiderResponse.fromJson(Map<String,dynamic>json){
    return SingleResultRiderResponse(
        json['msg'],
        json['code'],
        json['isSuccess'],
        RiderResponse.fromJson(json['data']));


  }


}
