import 'package:dio/dio.dart';
import 'package:practice_app/config/config_api.dart';
import 'package:practice_app/functions/token_lib.dart';
import 'package:practice_app/model/login_request.dart';
import 'package:practice_app/model/login_result.dart';
import 'package:practice_app/model/single_result_rider_response.dart';

class RepoRider {
  // 멤버 정보 불러오기
  Future<SingleResultRiderResponse> getRiderInfo(num memberId) async {
    String? token = await TokenLib.getRiderToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer' + token!;

    String _baseUrl = '$apiUri/rider/detail';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));

    return SingleResultRiderResponse.fromJson(response.data);
  }

  // 로그인
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {

    const String _baseUrl = '$apiUri/login/app/user';

    Dio dio = Dio();
    final response = await dio.post(
        _baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return LoginResult.fromJson(response.data);
  }
}