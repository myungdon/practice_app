import 'package:dio/dio.dart';
import 'package:practice_app/config/config_api.dart';
import 'package:practice_app/model/delivery_detail_result.dart';

class RepoDelivery {
  Future<DeliveryDetailResult> getDelivery(num id) async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/delivery/detail/{deliveryId}';

    final response = await dio.get(
        _baseUrl.replaceAll('{deliveryId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return DeliveryDetailResult.fromJson(response.data);
  }
}