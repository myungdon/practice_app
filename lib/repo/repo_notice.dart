import 'package:dio/dio.dart';
import 'package:practice_app/config/config_api.dart';
import 'package:practice_app/functions/token_lib.dart';
import 'package:practice_app/model/board_change_request.dart';
import 'package:practice_app/model/board_create_request.dart';
import 'package:practice_app/model/common_result.dart';
import 'package:practice_app/model/notice_list_result.dart';
import 'package:practice_app/model/single_result_notice_response.dart';

class RepoNotice {
  //공지사항  리스트  불러오기

  Future<NoticeListResult> getNoticeList() async {
    Dio dio = Dio();
    String _baseUri = '$apiUri/board/all';

    final response = await dio.get(
        _baseUri,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    print(response.data.toString());

    return NoticeListResult.fromJson(response.data);
  }

  //공지사항 단수 불러오기

  Future<SingleResultNoticeResponse> getNotice(num id) async{
    Dio dio = Dio();

    String _baseUri = '$apiUri/board/detail/{id}';

    final response = await dio.get(
        _baseUri.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )

    );
    return SingleResultNoticeResponse.fromJson(response.data);
  }



// 공지사항 등록

  Future<CommonResult> setBoard(BoardCreateRequest request) async {
    // 토큰 필요할 때 두 줄 살리기
    // String? token = await TokenLib.getRiderToken(); // 추가
    const String _baseUri = '$apiUri/board/new';

    Dio dio = Dio();
    // dio.options.headers['Authorization'] = 'Bearer' + token!; // 추가
    final response = await dio.post(
        _baseUri,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  // 공지사항 변경
  Future<CommonResult> putBoard(BoardChangeRequest request, num boardId) async {
    const String _baseUri = '$apiUri/board/change/{boardId}';

    Dio dio = Dio();
    final response = await dio.put(
        _baseUri.replaceAll('{boardId}', boardId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}