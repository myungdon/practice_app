import 'package:dio/dio.dart';
import 'package:practice_app/config/config_api.dart';
import 'package:practice_app/model/ask_list_result.dart';

class RepoAsk {
  Future<AskListResult> getAsks() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/ask/all';

    final response = await dio.get(
      _baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return AskListResult.fromJson(response.data);
  }
}