// import 'package:flutter/material.dart';
// import 'package:practice_app/components/component_notice_list.dart';
// import 'package:practice_app/config/config_color.dart';
// import 'package:practice_app/model/notice_item.dart';
// import 'package:practice_app/pages/page_notice_detail.dart';
// import 'package:practice_app/repo/repo_notice.dart';
//
// class PageNoticeList extends StatefulWidget {
//   const PageNoticeList({super.key});
//
//   @override
//   State<PageNoticeList> createState() => _PageNoticeListState();
// }
//
// class _PageNoticeListState extends State<PageNoticeList> {
//
//   List<NoticeItem> _list = [
//     // NoticeItem(1, '서비스 긴급 점검 안내', '2024-03-13'),
//     // NoticeItem(2, '서비스 긴급 점검 안내', '2024-03-13'),
//     // NoticeItem(3, '서비스 긴급 점검 안내', '2024-03-13'),
//     // NoticeItem(4, '서비스 긴급 점검 안내', '2024-03-13'),
//     // NoticeItem(5, '서비스 긴급 점검 안내', '2024-03-13')
//   ];
//
//   Future<void> _loadList() async {
//     await RepoNotice().getNoticeList()
//         .then((res) => {
//       setState((){
//         _list =res.list;
//       })
//     })
//         .catchError((err) => {
//       debugPrint(err)
//     });
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     _loadList();
//   }
//
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         leading: IconButton(
//           onPressed:(){
//             Navigator.of(context).pushNamed("/page_order_request");
//           },
//           icon:Icon(Icons.keyboard_backspace,color: Colors.white,),
//         ),
//         backgroundColor: colorPrimary ,
//         title: Text(
//           "공지사항",
//           style: TextStyle(
//               fontFamily: 'NotoSans_Bold',
//               fontSize: 16,
//               color: Colors.white,
//               letterSpacing: -0.5
//           ),
//         ),
//         centerTitle: true,
//       ),
//       body: ListView.builder(
//           itemCount: _list.length,
//           itemBuilder: (BuildContext ctx,int idx) {
//             return ComponentNoticeList(
//                 noticeItem: _list[idx],
//                 callback: () {
//                   Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageNoticeDetail(id: _list[idx].id)));
//                 }
//             );
//
//
//
//           }
//       ),
//       // body: SingleChildScrollView(
//       //   child: Container(
//       //     height: MediaQuery.of(context).size.height,
//       //     color: colorMainBg,
//       //     child: Column(
//       //       children: [
//       //         ComponentNoticeList(noticeItem:_list),
//       //
//       //       ],
//       //     ),
//       //   ),
//       // ),
//     );
//   }
// }
//
import 'package:flutter/material.dart';
import 'package:practice_app/components/component_main_appbar.dart';
import 'package:practice_app/components/component_notice.dart';
import 'package:practice_app/config/config_color.dart';
import 'package:practice_app/model/notice_item.dart';
import 'package:practice_app/pages/page_notice_detail.dart';
import 'package:practice_app/repo/repo_notice.dart';

class PageNoticeList extends StatefulWidget {
  const PageNoticeList({super.key});

  @override
  State<PageNoticeList> createState() => _PageNoticeListState();
}

class _PageNoticeListState extends State<PageNoticeList> {

  List<NoticeItem> _list = [
    // NoticeItem(1, '서비스 긴급 점검 안내', '2024-03-13'),
    // NoticeItem(2, '서비스 긴급 점검 안내', '2024-03-13'),
    // NoticeItem(3, '서비스 긴급 점검 안내', '2024-03-13'),
    // NoticeItem(4, '서비스 긴급 점검 안내', '2024-03-13'),
    // NoticeItem(5, '서비스 긴급 점검 안내', '2024-03-13')
  ];

  Future<void> _loadList() async {
    await RepoNotice().getNoticeList()
        .then((res) => {
      setState((){
        _list =res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentMainAppbar(appBar: AppBar(), center: true,),
      body: ListView.builder(
          itemCount: _list.length,
          itemBuilder: (BuildContext ctx,int idx) {
            return ComponentNotice(
                noticeItem: _list[idx],
                callback: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageNoticeDetail(id: _list[idx].id)));
                }
            );



          }
      ),
      // body: SingleChildScrollView(
      //   child: Container(
      //     height: MediaQuery.of(context).size.height,
      //     color: colorMainBg,
      //     child: Column(
      //       children: [
      //         ComponentNoticeList(noticeItem:_list),
      //
      //       ],
      //     ),
      //   ),
      // ),
    );
  }
}

