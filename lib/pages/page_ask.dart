import 'package:flutter/material.dart';
import 'package:practice_app/model/ask_item.dart';
import 'package:practice_app/repo/repo_ask.dart';

class PageAsk extends StatefulWidget {
  const PageAsk ({super.key});

  @override
  State<PageAsk> createState() => _PageAskState();
}

class _PageAskState extends State<PageAsk> with TickerProviderStateMixin{

  late TabController _tabController;

  List<AskItem> _askList = [
    AskItem(1, '주문1'),
    AskItem(2, '주문2'),
    AskItem(3, '주문3')
  ];

  Future<void> _loadAskList() async {
    await RepoAsk().getAsks()
        .then((res) =>
    {
      setState(() {
        _askList = res.list;
      })
    })
        .catchError((err) =>
    {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadAskList();
    _tabController = new TabController(length: 3, vsync: this);
  }


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(length: 3, child: Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(130),
        child: Scaffold(
          appBar: AppBar(title: Text("앱바임"),
            bottom: TabBar(
              controller: _tabController,
              tabs: [
                Tab(
                  text: '요청',
                  height: 50,
                ),
                Tab(
                  text: '배차',
                  height: 50,
                ),
                Tab(
                  text: '출발',
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
          children: [
            // 첫번째 탭 화면
            Container(
                child: Column(
                  children: [
                    Text("요청 화면"),
                    ElevatedButton(
                      child: Text("배차"),
                      onPressed: () {
                        setState(() {
                          _tabController.index = 1;
                        });
                      },
                    ),
                  ],
                )
            ),
            // 두번째 탭 화면
            Container(
              child: Column(
                children: [
                  Text("배차 화면"),
                  ElevatedButton(
                    child: Text("출발"),
                    onPressed: () {
                      setState(() {
                        _tabController.index = 2;
                      });
                    },
                  ),
                ],
              )
            ),
            // 세번째 탭 화면
            Container(
              child: Text("출발 화면"),
            )
          ],
        ),
    ));
  }
}
