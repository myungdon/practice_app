import 'package:flutter/material.dart';

class PageScreenA extends StatelessWidget {
  const PageScreenA({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScreenA'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/screen-b");
                },
                child: Text('Go to B')),
            SizedBox(
              height: 50,
            ),
            TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/screen-c");
                },
                child: Text('Go to C'))
          ],
        ),
      ),
    );
  }
}
