import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:practice_app/components/component_main_appbar.dart';
import 'package:practice_app/components/component_sub_appbar.dart';
import 'package:practice_app/config/config_color.dart';
import 'package:practice_app/config/config_size.dart';

class PageToast extends StatefulWidget {
  const PageToast({super.key});

  @override
  State<PageToast> createState() => _PageToastState();
}


class _PageToastState extends State<PageToast> {
  late FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    // if you want to use context from globally instead of content we need to pass navigatorKey.currentContext!
    fToast.init(context);
  }

  _showToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.greenAccent,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(Icons.check),
          SizedBox(
            width: 12.0,
          ),
          Text("This is a Custom Toast"),
        ],
      ),
    );


    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );

    // Custom Toast Position
    // fToast.showToast(
    //     child: toast,
    //     toastDuration: Duration(seconds: 2),
    //     positionedToastBuilder: (context, child) {
    //       return Positioned(
    //         child: child,
    //         top: 16.0,
    //         left: 16.0,
    //       );
    //     });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentSubAppbar(
        appBar: AppBar(),
        title: 'toast',
        center: true,
      ),
      body: Center(
        child: OutlinedButton(
          child: Text('Toast'),
          onPressed: () {
            _showToast();
          },
        ),
      ),
    );
  }
}