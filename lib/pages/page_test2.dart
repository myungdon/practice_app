import 'package:flutter/material.dart';
import 'package:practice_app/components/component_main_appbar.dart';
import 'package:practice_app/config/config_color.dart';

class PageTest2 extends StatefulWidget {
  const PageTest2({super.key});

  @override
  State<PageTest2> createState() => _PageTest2State();
}

class _PageTest2State extends State<PageTest2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentMainAppbar(
        appBar: AppBar(),
        center: true,
      ),
      body: SizedBox(
        width: 100,
        height: 100,
        child: TextButton(
          child: Text(
            'Show me',
            style: TextStyle(
              color: colorPrimary,
              backgroundColor: colorSecondary,
            ),
          ),
          onPressed: () {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(
                content: Text('Hellow'),
              duration: Duration(seconds: 5),
            ));
          },
        ),
      ),
    );
  }
}
