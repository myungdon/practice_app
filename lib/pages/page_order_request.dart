import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:practice_app/model/rider_response.dart';

class PageOrderRequest extends StatefulWidget {
  const PageOrderRequest({
    super.key,
    // required this.orderRequestListResult
    // required this.riderId
  });

  // final OrderRequestListResult orderRequestListResult;
  // final num riderId;

  @override
  State<PageOrderRequest> createState() => _PageOrderRequestState();
}

class _PageOrderRequestState extends State<PageOrderRequest> {
  bool _isWorked = false;

  RiderResponse? _response;

  // Future<void> _loadOrderContents() async {
  //   await RepoRider().getRiderInfo(widget.riderId)
  //       .then((res) => {
  //     setState(() {
  //       _response = res.data;
  //     })
  //   });
  // }
  //
  // void initState() {
  //   super.initState();
  //   _loadOrderContents();
  // }


  @override
  Widget build(BuildContext context) {
    double mediaWidth = MediaQuery.of(context).size.width;
    double mediaHeight = MediaQuery.of(context).size.height;

    return DefaultTabController(
      length: 5,
      child: Scaffold(
        /** 앱바 **/
        appBar: PreferredSize( /** 앱바 높이 지정 **/
          preferredSize: Size.fromHeight(130),
          child: AppBar(
            centerTitle: true,
            title: Image.asset(
              "./assets/img.png",
              width: MediaQuery.of(context).size.width * 0.23,
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/page_pay_main');
                },
                child: Text(
                  '85,600원',
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: 'NotoSans_NotoSansKR-Medium',
                      color: Colors.green,
                      letterSpacing: -0.5
                  ),
                ),
                style: TextButton.styleFrom(
                    foregroundColor: Colors.grey
                ),
              )
            ],
            bottom: TabBar(
              tabs: [
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        // '${widget.orderRequestListResult.totalCount}',
                        '15',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: 16
                        ),
                      ),
                      Text(
                        '요청',
                        style: TextStyle(
                          height: 0,
                        ),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '0',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: 16
                        ),
                      ),
                      Text(
                        '배차',
                        style: TextStyle(
                            height: 0
                        ),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '0',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: 16
                        ),
                      ),
                      Text(
                        '출발',
                        style: TextStyle(
                            height: 0
                        ),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '1',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: 16
                        ),
                      ),
                      Text(
                        '완료',
                        style: TextStyle(
                            height: 0
                        ),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '1',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: 16
                        ),
                      ),
                      Text(
                        '취소',
                        style: TextStyle(
                            height: 0
                        ),
                      ),
                    ],
                  ),
                )
              ],
              labelPadding: EdgeInsets.fromLTRB(0, 16, 0, 8), /** 탭바 패딩 **/
              labelColor: Colors.amber, /** 선택된 탭바 텍스트 컬러 **/
              labelStyle: TextStyle( /** 탭바 텍스트 **/
                fontFamily: 'NotoSans_Bold',
                fontSize: 20,
                letterSpacing: -0.5,
              ),
              unselectedLabelColor: Colors.amber, /** 선택 안된 탭바 텍스트 컬러 **/
              overlayColor: MaterialStatePropertyAll(Colors.amber), /** 클릭 이벤트 컬러 **/
              indicatorSize: TabBarIndicatorSize.tab, /** 탭바 선택 시 사이즈 **/
              indicator: BoxDecoration( /** 탭바 선택 시 배경 컬러 **/
                color: Colors.amber,
              ),
              dividerColor: Colors.amber, /** 탭 마다 밑줄 넣어주기 **/
              dividerHeight: 1.5, /** 탭 밑줄 굵기 **/
            ),
          ),
        ),
        /** 드로어 메뉴 **/
        drawer: Drawer(
          backgroundColor: Colors.amber,
          surfaceTintColor : Colors.amber,
          child: ListView(
            children: [
              SizedBox(
                height: 260,
                /** 메뉴 헤더 **/
                child: UserAccountsDrawerHeader(
                  margin: EdgeInsets.zero,
                  arrowColor: Colors.amber,
                  /** 프로필 아이콘 **/
                  currentAccountPicture: CircleAvatar(
                      child: Container(
                        margin: EdgeInsets.only(top: 15.0),
                        child: Icon(
                          Icons.account_circle,
                          size: 80,
                          color: Colors.amber,
                        ),
                      )
                  ),
                  /** 라이더 이름, 상태, 지역 텍스트 **/
                  otherAccountsPictures: [
                    Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(105, 15, 0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          // _response!.name,
                                            '홍길동',
                                            style: TextStyle(
                                                fontFamily: 'NotoSans_Bold',
                                                fontSize: 16,
                                                letterSpacing: -0.5,
                                                color: Colors.amber
                                            )
                                        ),
                                        Text(
                                            ' 라이더님',
                                            style: TextStyle(
                                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                                fontSize: 16,
                                                letterSpacing: -0.5,
                                                color: Colors.amber
                                            )
                                        ),
                                      ],
                                    ),
                                    Text(
                                        '정상업무 중입니다.',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: 16,
                                            letterSpacing: -0.5,
                                            color: Colors.blue
                                        )
                                    ),
                                    Text(
                                        '안산시 단원구',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: 16,
                                            letterSpacing: -0.5,
                                            color: Colors.blue
                                        )
                                    ),
                                  ],
                                )
                            ),
                            /** 업무 상태 스위치 **/
                            Container(
                              margin: EdgeInsets.fromLTRB(13, 15, 0, 0),
                              child: CupertinoSwitch( // ??????????????????????
                                  value: _isWorked,
                                  activeColor: Colors.redAccent,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isWorked = value ?? true;
                                    });
                                  }),
                            ),
                          ],
                        )
                    ),
                  ],
                  /** 라이더 이름, 상태, 지역 사이즈 **/
                  otherAccountsPicturesSize: MediaQuery.of(context).size / 1.55,
                  // otherAccountsPicturesSize: const Size.square(265.0),
                  /** 완료건, 페이 타이틀 **/
                  accountName: Container(
                      margin: EdgeInsets.fromLTRB(8, 40, 0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                child: Text(
                                    '오늘 총 완료건',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: 16,
                                        letterSpacing: -0.5,
                                        color: Colors.blue
                                    )
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(65, 0, 0, 0),
                                child: Text(
                                    '오늘의 페이',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: 16,
                                        letterSpacing: -0.5,
                                        color: Colors.blue
                                    )
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                  /** 완료건, 페이 수치 **/
                  accountEmail: Container(
                    margin: EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                              '2',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: 16,
                                  letterSpacing: -0.5,
                                  color: Colors.blue
                              )
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 139.0),
                          child: Text(
                              '85,600',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: 16,
                                  letterSpacing: -0.5,
                                  color: Colors.blue
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.red,
                  ),
                ),
              ),
              /** 메뉴 리스트 **/
              Container(
                margin: EdgeInsets.zero,
                child: Column(
                  children: [
                    ListTile(
                      title: Text(
                        '내 정보 관리',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {
                        // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageChangeMyInfo()));
                      },
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '페이 및 수입 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {
                        // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePayMain()));
                      },
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '공지사항',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {
                        // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageNoticeList()));
                      },
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '주문 과거 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {
                        // Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageOrderHistory()));
                      },
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        'VAN 설정',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {},
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '기능 설정',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {},
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '시간제 유상운송보험 신청',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {},
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '이용약관',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {},
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '라이더 인증 관리',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {},
                      tileColor: Colors.red,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '앱 버전 정보',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: 16,
                            letterSpacing: -0.5,
                            color: Colors.blue
                        ),
                      ),
                      onTap: () {},
                      tileColor: Colors.red,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        /** 탭바 컨텐츠 **/
        body: TabBarView(
          /** 구글맵 확대, 축소 **/
            physics: NeverScrollableScrollPhysics(),
            children: [
              /** 요청 감싸는 전체 틀 **/
              // PageRequestBuild(),
              /** 배차 감싸는 전체 틀 **/
              Stack(
                children: [
                  // Container(
                  //   color: Colors.red,
                  //   child: Column(
                  //     children: [
                  //       /** 구글맵 **/
                  //       Container(
                  //           height: MediaQuery.of(context).size.height / 2,
                  //           child: MapOrderGet()
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  // Container(
                  //     child: ComponentOrderGet(deliveryId: 1)
                  // )
                ],
              ),
              /** 출발 감싸는 전체 틀 **/
              Stack(
                children: [
                  Container(
                    color: Colors.red,
                    child: Column(
                      children: [
                        /** 구글맵 **/
                        Container(
                            height: MediaQuery.of(context).size.height / 2,
                            // child: MapOrderStart()
                        ),
                      ],
                    ),
                  ),
                  Container(
                      // child: ComponentOrderStartResponse(deliveryId: 1)
                  )
                ],
              ),
              /** 완료 감싸는 전체 틀 **/
              // PageOrderFinBuild(),
              /** 취소 감싸는 전체 틀 **/
              // PageOrderCancelBuild()
            ]
        ),
      ),
    );
  }
}

