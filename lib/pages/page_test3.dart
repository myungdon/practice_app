import 'package:flutter/material.dart';
import 'package:practice_app/components/component_main_appbar.dart';
import 'package:practice_app/config/config_color.dart';

class PageTest3 extends StatefulWidget {
  const PageTest3({super.key});

  @override
  State<PageTest3> createState() => _PageTest3State();
}

class _PageTest3State extends State<PageTest3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentMainAppbar(
        appBar: AppBar(),
        center: true,
      ),
      body: Center(
        child: TextButton(
          child: Text('Show me'),
          onPressed: () {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(
                  'Hellow',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: colorPrimary),
                ),
                backgroundColor: colorSecondary,
                duration: Duration(seconds: 1),
              ),
            );
          },
        ),
      ),
    );
  }
}
