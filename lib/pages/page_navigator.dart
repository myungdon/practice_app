import 'package:flutter/material.dart';
import 'package:practice_app/components/component_main_appbar.dart';
import 'package:practice_app/components/component_sub_appbar.dart';

class PageNavigator extends StatefulWidget {
  const PageNavigator({super.key});

  @override
  State<PageNavigator> createState() => _PageNavigatorState();
}

class _PageNavigatorState extends State<PageNavigator> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentMainAppbar(
        appBar: AppBar(),
        center: true,
      ),
      body: Center(
        child: TextButton(
          child: Text('go to 2th page'),
          onPressed: () =>
            Navigator.push(context, MaterialPageRoute(
                builder: (context) => SecondPage()))
        ),
      ),
    );
  }
}

class SecondPage extends StatelessWidget {
  const SecondPage({super.key});

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: ComponentSubAppbar(
        appBar: AppBar(),
        title: '2 page',
        center: true,
      ),
      body: Center(
        child: TextButton(
          child: Text('go to 1th page'),
          onPressed: () {
            Navigator.pop(ctx);
          },
        ),
      ),
    );
  }
}
