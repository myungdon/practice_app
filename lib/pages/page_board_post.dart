import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:practice_app/components/component_text_btn.dart';
import 'package:practice_app/config/config_form_validator.dart';
import 'package:practice_app/model/board_create_request.dart';
import 'package:practice_app/pages/page_board_post2.dart';

class PageBoardPost extends StatefulWidget {
  const PageBoardPost({Key? key}) : super(key: key);

  @override
  State<PageBoardPost> createState() => _PageBoardPostState();
}

class _PageBoardPostState extends State<PageBoardPost> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(15),
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: ListView(
            children: [
              const SizedBox(height:50),
              titleInput(),
              // imageInput(),
              const SizedBox(height: 15),
              submitButton(),
              const SizedBox(height: 15),
              cancelButton(),
            ],
          ),
        ),
      ),
    );
  }

  // 제목 입력
  Widget titleInput() {
    return FormBuilderTextField(
      name: 'title',
      maxLength: 20,
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(errorText: formErrorRequired),
        FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
        FormBuilderValidators.maxLength(50, errorText: formErrorMaxLength(50)),
      ]),
      keyboardType: TextInputType.text,
      autofocus: true,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: '제목 입력해주세요.',
        labelText: '제목',
        labelStyle: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget submitButton() {
    return ComponentTextBtn('다음', () {
      if(_formKey.currentState!.saveAndValidate()) {
        BoardCreateRequest request = BoardCreateRequest(
          _formKey.currentState!.fields['title']!.value
        );
        Navigator.push(context, MaterialPageRoute(builder: (context) => PageBoardPost2(title: request.title)));
      }
    }
    );
  }

  // 취소 버튼
  Widget cancelButton() {
    return ComponentTextBtn('취소', () { Navigator.of(context).pushNamed('/navigator'); });
  }
}
