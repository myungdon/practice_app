import 'package:flutter/material.dart';

class PageScreenC extends StatelessWidget {
  const PageScreenC({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScreenA'),
      ),
      body: Center(
        child: Text('ScreenC'),
      ),
    );
  }
}
