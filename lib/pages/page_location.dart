import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class PageLocation extends StatefulWidget {
  const PageLocation({super.key});

  @override
  State<PageLocation> createState() => _PageLocationState();
}

class _PageLocationState extends State<PageLocation> {

  Position? currentPosition; // 처음에 값이 없으니까 있어도 되고 업어도 되게

  @override
  void initState() {
    super.initState();
    _determinePosition().then((value) => {
      setState(() {
        currentPosition = value; // 원웨이 바인딩니까 안쳐다 봐서 값이 처음 상태 null임, 바꼇다고 해줘야함
      }),
    });
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("앱빠"),
      ),
      body: Text("위도: ${currentPosition?.latitude}, 경도: ${currentPosition?.longitude}"),
    );
  }
}
