import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:practice_app/components/component_text_btn.dart';
import 'package:practice_app/config/config_form_validator.dart';
import 'package:practice_app/model/board_create_request.dart';
import 'package:practice_app/repo/repo_notice.dart';

class PageBoardPost3 extends StatefulWidget {
  const PageBoardPost3({Key? key, required this.title, required this.text}) : super(key: key);

  final String title;
  final String text;

  @override
  State<PageBoardPost3> createState() => _PageBoardPost3State();
}

class _PageBoardPost3State extends State<PageBoardPost3> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setBoard(BoardCreateRequest request) async {
    await RepoNotice().setBoard(request).then((res) {
      request.title = '';
      request.text = '';
      request.img = '';
      // 페이지 이동을 부탁한다.
      Navigator.pushNamed(context, '/navigator');
    }).catchError((err) {
      debugPrint(err);
    });
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(15),
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: ListView(
            children: [
              SizedBox(height:50),
              imageInput(),
              const SizedBox(height: 15),
              submitButton(),
              const SizedBox(height: 15),
              cancelButton(),
              const SizedBox(height: 15),
            ],
          ),
        ),
      ),
    );
  }

  // 이미지 추가
  Widget imageInput() {
    return FormBuilderTextField(
      name: 'img',
      maxLength: 100,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: 'img 입력해주세요.',
        labelText: 'img',
        labelStyle: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  // 등록 버튼
  Widget submitButton() {
    return ComponentTextBtn('등록', () {
      if(_formKey.currentState!.saveAndValidate()) {
        BoardCreateRequest request = BoardCreateRequest(
          widget.title,
          widget.text,
          _formKey.currentState!.fields['img']?.value,
        );
        _setBoard(request);
      }
    }
    );
  }

  // 취소 버튼
  Widget cancelButton() {
    return ComponentTextBtn('취소', () { Navigator.of(context).pushNamed('/navigator'); });
  }
}
