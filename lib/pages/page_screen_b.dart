import 'package:flutter/material.dart';

class PageScreenB extends StatelessWidget {
  const PageScreenB({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ScreenA'),
      ),
      body: Center(
        child: Text('ScreenB'),
      ),
    );
  }
}
