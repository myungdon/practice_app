import 'package:flutter/material.dart';
import 'package:practice_app/components/component_divider.dart';
import 'package:practice_app/components/component_main_appbar.dart';
import 'package:practice_app/config/config_color.dart';
import 'package:practice_app/config/config_path.dart';

class PageTest1 extends StatefulWidget {
  const PageTest1({super.key});

  @override
  State<PageTest1> createState() => _PageTest1State();
}

class _PageTest1State extends State<PageTest1> {
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: colorBack,
        appBar: ComponentMainAppbar(appBar: AppBar(), center: true,),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              UserAccountsDrawerHeader(
                currentAccountPicture: CircleAvatar(
                  backgroundImage: AssetImage('assets/profile.jpg'),
                  backgroundColor: colorSecondary,
                ),
                otherAccountsPictures: [
                  CircleAvatar(
                    backgroundImage: AssetImage('assets/bonche.gif'),
                    backgroundColor: colorSecondary,
                  )
                ],
                accountName: Text('BBANTO'),
                accountEmail: Text('bbanto@bbanto.com'),
                onDetailsPressed: () {
                  print('arrow is clicked');
                },
                decoration: BoxDecoration(
                  color: Colors.red[200],
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40), bottomRight: Radius.circular(40))
                ),
              ),
              ListTile(
                leading: Icon(Icons.home,
                color: colorThird,
                ),
                title: Text('Home'),
                onTap: () {
                  print(('Home is clicked'));
                },
                trailing: Icon(Icons.add),
              ),
              ListTile(
                leading: Icon(Icons.settings,
                  color: colorThird,
                ),
                title: Text('Setting'),
                onTap: () {
                  print(('Setting is clicked'));
                },
                trailing: Icon(Icons.add),
              ),
              ListTile(
                leading: Icon(Icons.question_answer,
                  color: colorThird,
                ),
                title: Text('Q&A'),
                onTap: () {
                  print(('Q&A is clicked'));
                },
                trailing: Icon(Icons.add),
              ),
            ],
          ),
        ),
        body: const Padding(
          padding: EdgeInsets.fromLTRB(30, 40, 0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: CircleAvatar(
                  backgroundImage: AssetImage('${pathBase}bonche.gif'),
                  radius: 60,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ComponentDivider(),
              Text(
                'NAME',
                style: TextStyle(
                  color: colorSecondary,
                  letterSpacing: 2.0,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                'BBANTO',
                style: TextStyle(
                    color: colorSecondary,
                    letterSpacing: 2,
                    fontSize: 28,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                'POWER LEVEL',
                style: TextStyle(
                  color: colorSecondary,
                  letterSpacing: 2.0,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                '14',
                style: TextStyle(
                    color: colorSecondary,
                    letterSpacing: 2,
                    fontSize: 28,
                    fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                children: [
                  Icon(Icons.check_circle_outline),
                  SizedBox(
                    width: 10,
                  ),
                  Text('using lightsaber',
                  style: TextStyle(
                    fontSize: 16,
                    letterSpacing: 1
                  ),
                  ),
                ],
              ),
              Row(
                children: [
                  Icon(Icons.check_circle_outline),
                  SizedBox(
                    width: 10,
                  ),
                  Text('face hero',
                    style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 1
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Icon(Icons.check_circle_outline),
                  SizedBox(
                    width: 10,
                  ),
                  Text('fire flames',
                    style: TextStyle(
                        fontSize: 16,
                        letterSpacing: 1
                    ),
                  ),
                ],
              ),
              Center(
                child: CircleAvatar(
                  backgroundImage: AssetImage('${pathBase}profile.jpg'),
                  radius: 40,
                ),
              )
            ],
          ),
        ));
  }
}
