import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:practice_app/components/component_text_btn.dart';
import 'package:practice_app/config/config_form_validator.dart';
import 'package:practice_app/model/board_create_request.dart';
import 'package:practice_app/pages/page_board_post3.dart';

class PageBoardPost2 extends StatefulWidget {
  const PageBoardPost2({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<PageBoardPost2> createState() => _PageBoardPost2State();
}

class _PageBoardPost2State extends State<PageBoardPost2> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(15),
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: ListView(
            children: [
              SizedBox(height:50),
              textInput(),
              const SizedBox(height: 15),
              submitButton(),
              const SizedBox(height: 15),
              cancelButton(),
              const SizedBox(height: 15),
            ],
          ),
        ),
      ),
    );
  }

  // text 입력
  Widget textInput() {
    return FormBuilderTextField(
      obscureText: true,
      name: 'text',
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(errorText: formErrorRequired),
      ]),
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: 'text 입력해주세요.',
        labelText: 'text',
        labelStyle: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget submitButton() {
    return ComponentTextBtn('다음', () {
      if(_formKey.currentState!.saveAndValidate()) {
        BoardCreateRequest request = BoardCreateRequest(
            widget.title,
            _formKey.currentState!.fields['text']?.value
        );
        Navigator.push(context, MaterialPageRoute(builder: (context) => PageBoardPost3(title: request.title, text: request.text.toString())));
      }
    }
    );
  }

  // 취소 버튼
  Widget cancelButton() {
    return ComponentTextBtn('취소', () { Navigator.of(context).pushNamed('/navigator'); });
  }
}
