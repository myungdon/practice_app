import 'package:flutter/material.dart';

class PageDeliveryDetail extends StatefulWidget {
  const PageDeliveryDetail({super.key, required this.id});

  final num id;

  @override
  State<PageDeliveryDetail> createState() => _PageDeliveryDetailState();
}

class _PageDeliveryDetailState extends State<PageDeliveryDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text('${widget.id}'),
    );
  }
}
