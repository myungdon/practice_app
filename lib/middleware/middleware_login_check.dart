import 'package:flutter/material.dart';
import 'package:practice_app/functions/token_lib.dart';
import 'package:practice_app/pages/page_index.dart';
import 'package:practice_app/pages/page_login.dart';
import 'package:practice_app/pages/page_navigator.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getRiderToken();
    if (memberToken == null) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageLogin()),
              (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageNavigator()),
              (route) => false);
    }
  }
}